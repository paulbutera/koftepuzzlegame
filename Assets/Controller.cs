﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets._2D;

public class Controller : MonoBehaviour {

    public Camera2DFollow cam;

    public List<Unit> allUnits;
    public int activeUnitNumber = 0;
    public Unit activeUnit;

	// Use this for initialization
	void Start () 
    {
        activeUnit = allUnits[0];
        activeUnit.isActive = true;
        cam = Camera.main.GetComponent<Camera2DFollow>();

	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(Input.GetButtonDown("Left Bumper"))
        {
            if (activeUnitNumber > 0)
            {
                activeUnitNumber --;
            }
            else 
            {
                activeUnitNumber = allUnits.Count-1;
            }

            activeUnit = allUnits[activeUnitNumber];
            cam.target = activeUnit.gameObject.transform;
            foreach(Unit u in allUnits)
            {
                u.isActive = false;
            }
            activeUnit.isActive = true;
        }
        else if (Input.GetButtonDown("Right Bumper"))
        {
            if (activeUnitNumber < allUnits.Count-1)
            {
                activeUnitNumber ++;
            }
            else
            {
                activeUnitNumber = 0;
            }

            activeUnit = allUnits[activeUnitNumber];
            cam.target = activeUnit.gameObject.transform;
            foreach (Unit u in allUnits)
            {
                u.isActive = false;
            }
            activeUnit.isActive = true;
        }
	}
}
