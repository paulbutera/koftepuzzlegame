﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour {

    public bool isActive;

    protected float xSpeed;

    protected Rigidbody2D rb;

	// Use this for initialization
	protected void Start () 
    {
	    rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	protected void Update () 
    {
        if (isActive)
        {
            ControllerInput();
        }
	}
    protected void FixedUpdate() 
    {
        if (isActive)
        {
            rb.velocity = new Vector2(Input.GetAxis("Horizontal") * xSpeed, rb.velocity.y);
        }
    } 
    private void ControllerInput()
    {
        if(Input.GetButtonDown("A Button"))
        {
            AButtonPressed();
        }
    }

    public virtual void AButtonPressed(){}
}
